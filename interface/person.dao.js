"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var person_model_1 = require("../aula1/person.model");
var PersonDao = /** @class */ (function () {
    function PersonDao() {
    }
    PersonDao.prototype.insert = function (object) {
        console.log("salvando " + object.toString());
        return true;
    };
    PersonDao.prototype.update = function (object) {
        return true;
    };
    PersonDao.prototype.delete = function (id) {
        return true;
    };
    PersonDao.prototype.find = function (id) {
        return null;
    };
    PersonDao.prototype.findAll = function () {
        return [new person_model_1.Person('teste1'), new person_model_1.Person('teste2')];
    };
    return PersonDao;
}());
exports.PersonDao = PersonDao;
//# sourceMappingURL=person.dao.js.map