import { DaoInterface } from "./dao.interface";
import { Person } from "../aula1/person.model";

export class PersonDao implements DaoInterface{
    tableName: string;    
    
    insert(object: any): boolean {
        console.log(`salvando ${object.toString()}`)
        return true;
    }

    update(object: any): boolean {
        return true;
    }

    delete(id: number): boolean {
        return true;
    }

    find(id: number): Person {
        return null;
    }

    findAll(): Person[] {
        return [new Person('teste1'), new Person('teste2')]
    }


}